# Knowledge City LMS react.js version

## Requirements

You will need stable version of node.js. Also we are recommend you to using yarn as a package manager.

## Setup

Clone project. Navigate to project directory.
``
yarn
``

or if you using npm as a package manager:

``
npm i
``

Make a copy of .env.sample file. Change environment variables for your needs.

## Lunch bundler for development mode

``
yarn run dev
``

or if you using npm as a package manager:

``
npm run dev
``
