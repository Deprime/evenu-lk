import React, { useEffect, useState } from 'react';
import { Redirect, useLocation, Link as RouterLink } from "react-router-dom";

// Components
import Logo     from "@src/components/shared/Logo";
import Link     from '@src/components/ui/Link';

// Routes
import PageRoute    from "@src/components/routes/PageRoute";
import guest_routes from "@src/routes/guest";

const GuestLayout = () => {
  const location = useLocation();
  const [redirect, setRedirect] = useState(false);
  const [bottom_nav, setBottomNav] = useState([]);

  useEffect(() => {
    const guest_path_list = guest_routes.map(r => r.path);
    let path = location.pathname;
    path     = (path.charAt(path.length - 1) === '/') ? path.substring(0, path.length-1) : path;
    setRedirect(!guest_path_list.includes(path));

    if (!redirect) {
      setBottomNav(
        guest_routes.filter(r => r.path !== path)
      );
    }
  }, [location]);

  if (redirect) {
    return <Redirect to={guest_routes[0].path} />
  }

  return (
    <section className="container mx-auto">
      <div className="flex justify-center">
        <div className="mt-20vh w-[22rem]">
          <div className='py-6 flex justify-center'>
            <RouterLink to="signin">
              <Logo />
            </RouterLink>
          </div>

          <section>
            {guest_routes.map((route, i) => <PageRoute key={i} {...route} />)}
          </section>

          <div className='pt-4 flex justify-between'>
            { bottom_nav.map((route, i) => {
                return (
                  <Link to={route.name} className="mx-2" key={`l-${i}`}>
                    {route.title}
                  </Link>
                )
              })
            }
          </div>
        </div>
      </div>
    </section>
  )
}

export default GuestLayout;
