import React, {useState} from 'react';
import { useHistory, useLocation } from "react-router-dom";

// Stores
import  rootStore from "@src/store/Store.js";

// Components
import AppHeader from "@src/components/Header/AppHeader";

// Hooks
import { useRefreshToken }    from "@src/hooks/useRefreshToken";

// Routes
import PageRoute  from "@src/components/routes/PageRoute";
import app_routes from "@src/routes/app";

const PrivateLayout = () => {
  const history  = useHistory();
  const location = useLocation();

  if (!rootStore.user.getToken()) {
    history.push({
      pathname: "/auth",
      state: {
        from: location.pathname
      }
    });
    return null;
  }
  else {
    useRefreshToken();
  }

  return (
    <>
      <AppHeader />
      <section className="main-wrapper">
        {app_routes.map((route, i) => <PageRoute key={i} {...route} />)}
      </section>
    </>
  )
}

export default PrivateLayout;
