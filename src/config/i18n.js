import i18n                 from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector     from "i18next-browser-languagedetector";
import HttpApi              from "i18next-http-backend";

const api_address = import.meta.env.VITE_API_URL;
const api_version = import.meta.env.VITE_API_VERSION;
const lang        = JSON.parse(window.localStorage.getItem('portal'))?.default_lang || 'en';

i18n
  .use(initReactI18next)
  .use(LanguageDetector)
  .use(HttpApi)
  .init({
    initImmediate: true,
    lng: lang,
    fallbackLng: lang,
    detection:{
      lookupLocalStorage: 'userOptionsStorage',
      order: ['path',' htmlTag', 'cookie', 'sessionStorage', 'navigator', 'querystring', 'subdomain'],
      caches: ['localStorage'],
    },
    ns:'default',
    defaultNS: "default",
    fallbackNS: "default",
    backend:{
      loadPath: api_address + `/${api_version}/mui?lang={{lng}}&groups%5B%5D={{ns}}`,
      // addPath:  api_address + `/${api_version}/mui?lang={{lng}}&groups%5B%5D={{ns}}`,
      allowMultiLoading: false,
      parse: function (data){
        data = JSON.parse(data)
        let translateObj = {};
        data.response.forEach((el) => {
          if (!translateObj.hasOwnProperty(el.key)) {
            translateObj[el.key] = el.value
          }
        })
        return data = translateObj
      }
    },
    interpolation: {
      // react already safes from xss => https://www.i18next.com/translation-function/interpolation#unescape
      escapeValue: false
    },
    react: {
      useSuspense: true
    }
  });

export default i18n
