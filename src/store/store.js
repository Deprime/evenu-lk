// import React from 'react';
// import {makeAutoObservable} from "mobx";

import User from "./User.js";

class Store {
  constructor() {
    this.user =  new User(this);
  }
}

const store = new Store();
export default store;
