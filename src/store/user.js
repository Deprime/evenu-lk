import {makeAutoObservable, toJS} from "mobx";
import { makePersistable } from 'mobx-persist-store';
// import store from "@src/store/Store.js";

import SecureLS from "secure-ls";
const ls = new SecureLS({isCompression: false});
// const ls = localStorage;

export default class User {
  // token = null;
  // user  = {};
  translation_mode = false;
  isAuth = null;
  lsKey = 'user'

  constructor () {
    makeAutoObservable(this);
    makePersistable(this, { name: 'translation_mode', properties: ['translation_mode'], storage: window.localStorage });
    makePersistable(this, { name: this.lsKey, properties: ['token', 'user'], storage: window.localStorage });
  }

  setTranslationMode(mode) {
    this.translation_mode = mode;
  }

  isTranslationMode() {
    return this.translation_mode;
  }

  isQuestionRepository() {
    let data = this.getAccountData()
    return data?.account_setting?.quiz?.isAddFromQuestionRepository
  }

  /**
   * Set user data
   * @param {*} data
   */
  set(data) {
    ls.set(this.lsKey, data);
  }

  /**
   * Get user data
   * @param {*} key
   * @returns
   */
  get(key = null) {
    return ls.get(this.lsKey);

    // const data = JSON.parse(ls.getItem('user_data')) ;
    // return(data && key) ? data[key] : data;
  }

  /**
   * Remove user data
   */
  clear() {
    ls.removeAll();
  }

  setToken(token) {
    ls.set('token', token);
  }

  getToken() {
    return ls.get('token');
  }

  setAuth(boolean) {
    this.isAuth = boolean;
  }

  setAuthorization(data) {
    this.set(data.user);
    this.setToken(data.token);
    this.setAuth(true);
  }
}
