import React, {createContext, useContext} from "react";

const StoreContext = createContext(null);

export const StoreProvider = ({ value, children }) => {
  return <StoreContext.Provider value={value}>{children}</StoreContext.Provider>
}

export const useStore = (context = StoreContext) => {
    const store = useContext(context)
    if (!store) {
        throw new Error('useStore must be used within a StoreProvider')
    }
    return store
}
