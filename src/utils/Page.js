const app_name = import.meta.env.VITE_APP_NAME

const Page = {
  /**
   * Set page title
   * @param {*} title
   * @returns
   */
  setTitle: (title) => {
    const page_title = `${title} :: ${app_name}`;
    document.title = page_title;
    return page_title;
  }
}

export default Page
