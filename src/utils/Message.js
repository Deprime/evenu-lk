import {message} from "antd";

export const Message = {
  sendError: (content) => {
    message.error({content: content, className: "kc-message-error"});
  },

  sendSuccess: (content) => {
    message.success({content: content, className: "kc-message-success"});
  }
}
