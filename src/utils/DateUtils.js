export const DateUtils = {
  /**
   * Returns today date in format YYYY/MM/DD
   * @returns string
   */
  todayFormatted: () => {
    const today = new Date();
    const day   = ("0" + today.getDate()).slice(-2);
    const month = ("0" + (today.getMonth() + 1)).slice(-2);
    return `${today.getFullYear()}/${month}/${day}`
  }
}
