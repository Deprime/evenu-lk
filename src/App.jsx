import React, { useState, useEffect, Suspense } from 'react';
import { StoreProvider } from "@src/store/Context.jsx";
import rootStore    from "@src/store/Store.js";

// Api endpoints
// import portalsApi   from "@src/services/api/Portals";

// Components
import LmsApp     from "@src/LmsApp.jsx";
import Loader     from "@src/components/ui/Loader";
import AppError   from "@src/components/ui/AppError";

// Styles
import './index.css'
import '@src/assets/scss/app.scss'

function App() {
  const [store,   setStore]   = useState();
  const [isReady, setIsReady] = useState(false);
  const [isInitialize, setInitialize] = useState(true);

  useEffect(async()=>{
    // const config  = await portalsApi.config()
    // const data    = await config.data.response
    setStore(rootStore);
    setIsReady(true);
  }, [])

  if (!isReady) {
    return <Loader />;
  }

  if (!isInitialize) {
    return <AppError />;
  }

  return (
    <Suspense fallback={<Loader />}>
      <StoreProvider value={store}>
        <LmsApp />
      </StoreProvider>
    </Suspense>
  )
}

export default App;
