// import $axios_v2_api from "@src/config/axios";
import { get, post } from "@src/config/axios";
import store from "@src/store/Store";

export const Auth = {
  /**
   * Sign in
   * @param {*} username
   * @param {*} password
   * @returns {Promise}
   */
  signin: (email, password) => {
    // Base auth params
    const url = '/auth/signin'
    return post(url, {email, password});
  },

  /**
   * Logout
   * @returns {Promise}
   */
  logout: () => post('/auth', {_method: 'delete'}),

  /**
   * Password recovery
   * @param login
   * @returns {Promise}
   */
  recovery: (login) => get(`/users/password/recovery?type=admin&login=${login}`, {
    params: {
      type: 'admin',
      login
    }
  }),

  /**
   * Refresh token
   * @description authorized method only
   * @returns {Promise}
   */
  refreshToken: () => post('/auth', {_method: 'put'}),
}

export default Auth;
