// Page components
import Dashboard    from "@src/pages/Dashboard";
import LogOut       from "@src/pages/LogOut.jsx";

const appRouteList = [
  {
    name: 'dashboard',
    path: '/dashboard',
    component: Dashboard,
  },
  // {
  //   name: 'notifications',
  //   path: '/admin2/notifications',
  //   component: Notifications,
  //   routes: [
  //     {
  //       name: 'notificationsEdit',
  //       path: '/admin2/notifications/:id',
  //       component: Edit
  //     }
  //   ]
  // },
  {
    name: 'logout',
    path: '/logout',
    component: LogOut
  }
];

export default appRouteList;
