// Page components
import Signin           from '@src/pages/Signin';
import RestorePassword  from '@src/pages/RestorePassword';
import Registration     from '@src/pages/Registration';

const guest_rotues = [
  {
    name: 'signin',
    path: '/signin',
    component: Signin,
    title: 'Авторизация',
  }, {
    name: 'registration',
    path: '/registration',
    component: Registration,
    title: 'Регистрация',
  }, {
    name: 'restore-password',
    path: '/restore-password',
    component: RestorePassword,
    title: 'Забыли пароль?',
  },
];

export default guest_rotues;
