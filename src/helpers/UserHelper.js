import React from 'react';


const UserHelper =  {
  userFields              :[
    'id',
    'api_user_id',
    'user_type',
    'login',
    'first_name',
    'last_name',
    'title',
    'account_id',
    'account',
    'account_setting',
    'groups',
    'token',
    'accessByPages',
    'isTrainingLimit',
    'isCoursesConfirmation',
    'accessByPages',
    'studentInfo',
    'options',
    'time_zone'
  ],

  userLocal: function (data){

    const dataObject       = {};

    //typical params
    this.userFields.forEach( function (key, i) {
      dataObject[key]  = (!!data[key]) ? data[key] : null;
    });

    //special prepare
    dataObject.account   = (data['account']) ? data['account'] : '';

    //extend typical data from user information
    Object.assign(dataObject,  {...data.user})

    dataObject['isTrainingLimit'] = (!!data['is_training_limit']) ?
      data['is_training_limit'] : null;

    dataObject['isCoursesConfirmation'] = (!!data['isCoursesConfirmation']);

    dataObject['time_zone'] = data.account_setting?.time_zone;

    return dataObject
  }


}
export default UserHelper
