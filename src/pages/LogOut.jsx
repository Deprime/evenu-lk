import React from "react";
import apiAuth from "@src/services/api/Auth.js";
import store from "@src/store/Store.js";
import {Redirect, useHistory, useLocation} from "react-router-dom";

const LogOut=()=>{
  let {isAuth, setAuth, remove,checkAuth} = store.user

  const history  = useHistory();
  const location = useLocation();

  store.user.setAuth(false);
  apiAuth.logout();
  store.user.remove();

  // console.log(isAuth);

  // history.push({pathname: "auth"});
  // history.go(0);

  return <Redirect from={location.state?.from}  to={'/admin2/auth'} />

}
export default  LogOut
