import React from "react";
import {Button} from "antd";
import MuiTag from "@src/components/shared/MuiTag";

const RestoreResult = ({setIsModalVisible}) => {
  return (
    <div>
      <div className="auth__text mb-6">
        <MuiTag muiKey={`LMS2Default:restore-result_text`} default={"Please check your email<br/>for password recovery instructions"} />
      </div>
      <Button
        className="auth__restore-close-btn"
        htmlType="button"
        type="primary"
        onClick={() => setIsModalVisible(false)}
      >
        <MuiTag muiKey={`LMS2Default:restore-result_back`} default={"Back to signing in"} />
      </Button>
    </div>
  )
}

export default RestoreResult
