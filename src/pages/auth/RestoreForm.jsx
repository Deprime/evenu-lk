import React from "react";
import {Button, Form, Input, Space} from "antd";
import Auth from "@src/services/api/Auth";
import {UserIcon} from "@src/assets/icons/icons-pack";
import MuiTag from "@src/components/shared/MuiTag";

const RestoreForm = ({setIsModalVisible, setIsResultVisible}) => {
  const [form] = Form.useForm();

  /**
   * Restore password
   */
  const handleRestoreSubmit = (values) => {
    Auth.recovery(values.restore_username).then(result => {
      if (result.status === 200) {
        setIsResultVisible(true);
      }
    });
  }

  return (
    <Form
      name="basic"
      autoComplete="off"
      form={form}
      layout="vertical"
      onFinish={handleRestoreSubmit}
    >
      <Form.Item
        label={<MuiTag muiGroup={'LMS2Default'} muiKey={`restore-hint`} default={"Enter username related to your account"} />}
        name="restore_username"
      >
        <Input prefix={<UserIcon width="12px" height="12px" />} />
      </Form.Item>
      <Form.Item shouldUpdate>
        {
          () => (
            <Space size="middle">
              <Button
                className="auth__sign-in-btn"
                htmlType="button"
                onClick={() => setIsModalVisible(false)}
              >
                <MuiTag muiGroup={'LMS2Default'} muiKey={`restore-reset`} default={"Reset"} />
              </Button>
              <Button
                className="auth__sign-in-btn"
                type="primary"
                htmlType="submit"
                disabled={!!!form.getFieldsValue()?.restore_username}
              >
                <MuiTag muiKey={`LMS2Default:restore-submit`} default={"Submit"} />
              </Button>
            </Space>
          )
        }
      </Form.Item>
    </Form>
  )
}

export default RestoreForm
