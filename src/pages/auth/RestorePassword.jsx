import React, {useState} from "react";
import {Modal} from "antd";
import RestoreResult from "@src/pages/auth/RestoreResult";
import RestoreForm from "@src/pages/auth/RestoreForm";
import MuiTag from "@src/components/shared/MuiTag";

const RestorePassword = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [isResultVisible, setIsResultVisible] = useState(false);

  return (
    <div className="auth__action-link">
      <span onClick={() => setIsModalVisible(true)}><MuiTag muiKey={`restore-link`} muiGroup={'LMS2Default'} muiDefault={"Restore password"} /></span>
      <Modal
        centered
        visible={isModalVisible}
        footer={null}
        closable={false}
        width={400}
      >
        <div className="auth__modal-title"><MuiTag muiKey={`restore-title`} muiGroup={'LMS2Default'} muiDefault={"Password restore"} /></div>
        {
          isResultVisible ?
            <RestoreResult setIsModalVisible={setIsModalVisible} /> :
            <RestoreForm setIsModalVisible={setIsModalVisible} setIsResultVisible={setIsResultVisible} />
        }
      </Modal>
    </div>
  )
}

export default RestorePassword;
