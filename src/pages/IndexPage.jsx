import React, {useEffect, useState} from "react";
// import {PieChart, Pie, Cell} from "recharts";
import {observer} from "mobx-react-lite";
import {useStore} from "@src/store/Context.jsx";
import PieChart from '@src/components/shared/PieChart';
import {useTranslation} from "react-i18next";
import {usePageTitle} from "@src/hooks/usePageTitle";

function IndexPage () {
  const page_title = 'Dashboard';
  usePageTitle(page_title);

  const store = useStore();
  const {t,i18n} = useTranslation('LMSPage')
  const [isReady, setReady] = useState(false)

  useEffect(async () => {
    setReady(false)
    // await store.licenseUsers.loadData()
    // setLicenseUsageData(store.licenseUsers.data)
    setReady(true)
  }, []);

  return (
    isReady ? (
      <div>
        <h2>{t("index-title")}</h2>
      </div>
    ) : <div>Loading</div>
  );
}

export default observer(IndexPage);
