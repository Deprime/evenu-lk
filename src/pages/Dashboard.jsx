import React, {useEffect, useState} from 'react';
// import { observer } from "mobx-react-lite";
import { useHistory, useLocation, Redirect } from "react-router-dom";

// Api services
import Auth from "@src/services/api/Auth";

// Stores
import store from "@src/store/Store";

// Hooks
import { usePageTitle } from "@src/hooks/usePageTitle";

// Components
// import RestorePassword from "@src/pages/auth/RestorePassword";
import Input      from "@src/components/ui/Input";
import Button     from "@src/components/ui/Button";
import FormGroup  from "@src/components/ui/FormGroup";
import Alert      from '../components/ui/Alert';
import Logo       from "@src/components/shared/Logo";

// Routes
// import { RedirectFromAuth } from "@src/routes/authRedirect.jsx";

// Assets
// import "./Dashboard.scss";

/**
 * Dashboard page
 * @returns {Render}
 */
function Dashboard() {
  const page_title  = 'Dashboard page';
  usePageTitle(page_title);

  // Data
  let path = null;

  // States
  const [form, setFormData] = useState({
    email: '',
    password: '',
  });
  const [loading, setLoading] = useState(false);
  const [errors, setError] = useState([]);
  const history  = useHistory();
  const location = useLocation();

  /**
   * Redirect on the previous page if the page is from this app
   */
  useEffect(() => {
    path = (location.state?.from) ? location.state.from : '/index/';
  });

  /**
   * Handle input change
   * @param {Object} event
   * @returns {Void}
   */
  const change = (e) => {
    const value     = e.target.value;
    const inputName = e.target.name;
    form[inputName] = value;
    setFormData({...form});
  }

  /**
   * Handle login event
   * @param {*} e
   * @returns {Void}
   */
  const login = (e) => {
    e.preventDefault();
    setLoading(true);
    Auth.signin(form.email, form.password)
      .then((r) => {
        console.log(r)
        const data = r.data;
        store.user.setAuthorization(data);

        // Redirect to previous page
        // handleRedirect();
      })
      .then(() => {
        console.log(store.user.get())
        console.log(store.user.getToken())
      })
      .catch((e) => {
        if (e?.response?.data?.errors) {
          const err = e.response.data.errors;
          let errorArray = [];
          Object.keys(err)
            .forEach((k) => {
              errorArray = [...errorArray, ...err[k]];
            });
          setError(errorArray);
        }
        else {
          console.error(e);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }


  return (
    <section className="container mx-auto">
      <div className="flex justify-center">
        <div className="mt-20vh w-[20rem]">
          <div className='py-6 flex justify-center'>
            <Logo />
          </div>
          <div>
            <h3>Dashboard</h3>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Dashboard;
