import React from 'react';
import { Button, Table, Tag, Space } from 'antd';
import {useTranslation} from "react-i18next";

// Data
const app_version = import.meta.env.VITE_APP_VERSION;
const page_title = 'Activity log';

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    render: text => <a>{text}</a>,
  },
  {
    title: 'Age',
    dataIndex: 'age',
    key: 'age',
  },
  {
    title: 'Address',
    dataIndex: 'address',
    key: 'address',
  },
  {
    title: 'Tags',
    key: 'tags',
    dataIndex: 'tags',
    render: tags => (
      <>
        {tags.map(tag => {
          let color = tag.length > 5 ? 'geekblue' : 'green';
          if (tag === 'loser') {
            color = 'volcano';
          }
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: 'Action',
    key: 'action',
    render: (text, record) => (
      <Space size="middle">
        <a>Invite {record.name}</a>
        <a>Delete</a>
      </Space>
    ),
  },
];
const data = [
  {
    key: '1',
    name: 'John Brown',
    age: 32,
    address: 'New York No. 1 Lake Park',
    tags: ['nice', 'developer'],
  },
  {
    key: '2',
    name: 'Jim Green',
    age: 42,
    address: 'London No. 1 Lake Park',
    tags: ['loser'],
  },
  {
    key: '3',
    name: 'Joe Black',
    age: 32,
    address: 'Sidney No. 1 Lake Park',
    tags: ['cool', 'teacher'],
  },
];

const MainPage = () => {
  const {t,i18n} = useTranslation('LMSPortalSettings')
  // const store = useStore();
  return (
    <main className="page">
      <section className="list-tools">
        <Button type="primary">
          Create new student
        </Button>
      </section>

      <Table columns={columns} dataSource={data}  size="small" />
      <div>Api version: {app_version}</div>
      <div>{t('main_library_background')}</div>
    </main>
  )
}
export default MainPage
