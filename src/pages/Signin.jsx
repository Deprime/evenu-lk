import React, {useEffect, useState} from 'react';
// import { observer } from "mobx-react-lite";
import { useHistory, useLocation, Redirect } from "react-router-dom";

// Api services
import Auth from "@src/services/api/Auth";

// Stores
import store from "@src/store/Store";

// Hooks
import { usePageTitle } from "@src/hooks/usePageTitle";

// Components
// import RestorePassword from "@src/pages/auth/RestorePassword";
import Input      from "@src/components/ui/Input";
import Button     from "@src/components/ui/Button";
import FormGroup  from "@src/components/ui/FormGroup";
import Alert      from '@src/components/ui/Alert';

// Routes
// import { RedirectFromAuth } from "@src/components/routes/authRedirect.jsx";

// Assets
// import "./Signin.scss";

/**
 * Sign in page
 * @returns {Render}
 */
function Signin() {
  const page_title  = 'Sign in to LMS';
  usePageTitle(page_title);

  // Data
  let path = null;

  // States
  const [form, setFormData] = useState({
    email: '',
    password: '',
  });
  const [loading, setLoading] = useState(false);
  const [errors, setError] = useState([]);
  const history  = useHistory();
  const location = useLocation();

  /**
   * Redirect on the previous page if the page is from this app
   */
  useEffect(() => {
    path = (location.state?.from) ? location.state.from : '/index/';
  });

  /**
   * Handle input change
   * @param {Object} event
   * @returns {Void}
   */
  const change = (e) => {
    const value     = e.target.value;
    const inputName = e.target.name;
    form[inputName] = value;
    setFormData({...form});
  }

  /**
   * Handle login event
   * @param {*} e
   * @returns {Void}
   */
  const login = (e) => {
    e.preventDefault();
    setLoading(true);
    Auth.signin(form.email, form.password)
      .then((r) => {
        console.log(r)
        const data = r.data;
        store.user.setAuthorization(data);

        // Redirect to previous page
        // handleRedirect();
      })
      .then(() => {
        console.log(store.user.get())
        console.log(store.user.getToken())
      })
      .catch((e) => {
        if (e?.response?.data?.errors) {
          const err = e.response.data.errors;
          let errorArray = [];
          Object.keys(err)
            .forEach((k) => {
              errorArray = [...errorArray, ...err[k]];
            });
          setError(errorArray);
        }
        else {
          console.error(e);
        }
      })
      .finally(() => {
        setLoading(false);
      });
  }

  const handleRedirect = () => {
    if (location.state?.from) {
      history.push(location.state.from);
      history.go(0);
    }
    else {
      history.push('/admin2/index');
    }
  }

  return (
    <form onSubmit={login}>
      <FormGroup>
        <Input
          nativeType="email"
          placeholder="Email"
          name="email"
          label="Введите ваш email"
          value={form.email}
          disabled={loading}
          onChange={change}
        />
      </FormGroup>
      <FormGroup>
        <Input
          nativeType="password"
          name="password"
          placeholder="Пароль"
          label="Введите ваш пароль"
          value={form.password}
          disabled={loading}
          onChange={change}
        />
      </FormGroup>

      <div className='py-2'>
        <Button
          variant="primary"
          nativeType="submit"
          loading={loading}
          block
        >
          Войти
        </Button>
      </div>

      {errors.length > 0 &&
        <Alert variant='danger'>
          <ul className='list-disc ml-4'>
            {errors.map((err, i) =>
              <li key={`err-${i}`}>{err}</li>
            )}
          </ul>
        </Alert>
      }
    </form>
  )
}

export default Signin;
