// import React, {useState} from 'react';
import {observer} from 'mobx-react-lite';


// Assets
import logo from '@src/assets/images/logo.svg'
import './AppHeader.scss'


const AppHeader = () => {
  return (
    <header className="header">
      <img src={logo} className="app-logo" alt="logo" />
    </header>
  )
}
export default observer(AppHeader)
