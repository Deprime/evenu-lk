import React from 'react';
import { Redirect } from 'react-router-dom';
import store from '@src/store/Store.js';

export const RedirectFromAuth = ({path}) => {
  const { isAuth } = store.user
  path = path ? path : '/index/';

  return (
    <>
      {isAuth ? <Redirect to={path}/> : <Redirect to={'/logout'}/>}
    </>
  );
}
