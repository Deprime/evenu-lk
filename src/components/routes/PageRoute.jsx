import React from 'react';
import { Route, Switch } from "react-router-dom";

const PageRoute = (props) => {
  const route_props = {
    key: props.name,
    path: props.path,
    component: props.component
  }

  if (props?.routes) {
    return (
      <Switch>
        <Route
          {...route_props}
          exact
        />
        { props.routes.map((route, i) => <PageRoute key={i} {...route} />) }
      </Switch>
    )
  }

  return <Route {...route_props} />
}

export default PageRoute;
