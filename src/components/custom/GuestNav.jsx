import React, { useEffect, useState } from 'react';

// Components
import Link     from '../components/ui/Link';

// Pages
import Signin           from '@src/pages/Signin';
import RestorePassword  from '@src/pages/RestorePassword';
import Registration     from '@src/pages/Registration';

const guest_routes = [
  {
    name: 'signin',
    path: '/signin',
    component: Signin,
    title: 'Авторизация',
  }, {
    name: 'restore-password',
    path: '/restore-password',
    component: RestorePassword,
    title: 'Забыли пароль?',
  }, {
    name: 'registration',
    path: '/registration',
    component: Registration,
    title: 'Регистрация',
  },
];

const GuestNav = () => {
  const location = useLocation();
  const [bottom_nav, setBottomNav] = useState([]);

  useEffect(() => {
    const guest_path_list = guest_routes.map(r => r.path);
    let path = location.pathname;
    path     = (path.charAt(path.length - 1) === '/') ? path.substring(0, path.length-1) : path;
    setBottomNav(
      guest_routes.filter(r => r.path !== path)
    );
  }, [location]);

  return (
    <div className='pt-4 flex justify-between'>
      { bottom_nav.map((route, i) => {
          return (
            <Link to={route.name} className="mx-2" key={`l-${i}`}>
              {route.title}
            </Link>
          )
        })
      }
    </div>
  )
}

export default GuestNav;
