import React, {useState, useEffect} from 'react';
import {useStore} from "@src/store/Context.jsx";
import {useTranslation} from "react-i18next";
import { observer } from "mobx-react-lite";
import {Tooltip} from 'react-tippy';
import {BookIcon} from "@src/assets/icons/icons-pack.jsx";

// styles
import './MuiTag.scss';

/**
 * @todo The Mui component takes two parameters: group and key.
 * It would be nice to make it so that the group does not need to be use but take from parent component.
 *
 * @param props
 * @return {JSX.Element}
 * @constructor
 */
const MuiTag = ({muiKey, muiGroup = "L2Default", muiDefault}) => {
  const store = useStore();
  // const [isShown, setIsShown] = useState(false);
  const { t } = useTranslation();

  // Store: translation mode
  const translation_mode = store.user.isTranslationMode();
  useEffect(() => {}, [translation_mode]);

  /**
   * Handle element click
   */
  function click(e) {
    console.log(props);
  };

  return (
    translation_mode
    ? (
        <Tooltip
          size="small"
          position="right"
          interactive
          theme="light"
          animation="perspective"
          html={<span className="clickable">Edit</span>}
        >
          <mark className="mui-tag" onClick={(e) => {click(e)}}>
            {/*{t(props.muiKey, props.default)}*/}
            {t(`${muiKey}`, {ns: muiGroup, defaultValue: muiDefault})}
          </mark>
        </Tooltip>
      )
    : (
      <mark className="clean-mark">
        {/*{t(.muiKey, props.default)}*/}
        {t(`${muiKey}`, {ns: muiGroup, defaultValue: muiDefault})}
      </mark>
    )
  )
};

export default observer(MuiTag);
