import React, { useEffect } from "react";
import logo from "@src/assets/images/logo.svg";

const Logo = ({...props}) => {

  useEffect(() => {});

  return (
    <img src={logo} {...props} className="app-logo" alt="logo" />
  );
}

export default Logo;
