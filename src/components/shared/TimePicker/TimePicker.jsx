import React, {useEffect, useState} from 'react';
import {Button, Input, Popover, Slider, Space} from "antd";
import InputTitle from "@src/components/shared/InputTitle/InputTitle";
import "./TimePicker.scss";
import {InputClockIcon} from "@src/assets/icons/icons-pack";
import MuiTag from "@src/components/shared/MuiTag";

const TimePicker = ({ disabled, initialHours, initialMinutes, onChange, width = "7.5rem" }) => {
  const [hours, setHours] = useState(initialHours);
  const [minutes, setMinutes] = useState(initialMinutes);

  useEffect(() => {
    if (initialMinutes !== minutes) {
      setMinutes(initialMinutes);
    }

    if (initialHours !== hours) {
      setHours(initialHours);
    }
  }, [initialHours, initialMinutes])

  const handleHoursChange = hours => {
    if (hours && hours.length === 3) {
      hours = hours.slice(1);
    }

    if (hours && (!Number.isInteger(+hours) || hours > 23)) {
      setHours("0");
    } else {
      setHours(hours);
    }

    onChange(getTime(hours, minutes));
  }

  const handleMinutesChange = minutes => {

    if (minutes && minutes.length === 3) {
      minutes = minutes.slice(1);
    }

    if (minutes && (!Number.isInteger(+minutes) || minutes> 59)) {
      setMinutes("0");
    } else {
      setMinutes(minutes);
    }

    onChange(getTime(hours, minutes));
  }

  const time = (
    <div>
      <div className="mb-4">
        <Space size={4}>
          <Input
            style={{width: "40px"}}
            onChange={e => handleHoursChange(e.target.value)}
            value={hours}
          />
          <span>:</span>
          <Input
            style={{width: "40px"}}
            onChange={e => handleMinutesChange(e.target.value)}
            value={minutes}
          />
        </Space>
      </div>
      <div className="mb-4">
        <InputTitle size="small"><MuiTag muiKey="hours" muiGroup="L2Default" muiDefault="Hours" /></InputTitle>
        <Slider
          onChange={value => handleHoursChange(value.toString())}
          min={0}
          max={23}
          value={+hours}
        />
      </div>
      <div className="mb-4">
        <InputTitle size="small"><MuiTag muiKey="minutes" muiGroup="L2Default" muiDefault="Minutes" /></InputTitle>
        <Slider
          onChange={value => handleMinutesChange(value.toString())}
          min={0}
          max={59}
          value={+minutes}
        />
      </div>
    </div>

  );

  const getTime = (hours, minutes) => {
    let result = "";

    if (!hours && !minutes) {
      return result;
    }

    if (hours.length === 1) {
      result += "0" + hours;
    } else if (!hours) {
      result += "00";
    } else {
      result += hours;
    }

    result += ":";

    if (minutes.length === 1) {
      result += "0" + minutes;
    } else if (!minutes) {
      result += "00";
    } else {
      result += minutes;
    }

    return result;
  }

  if (disabled) {
    return (
      <Input style={{width: width}} value={getTime(hours, minutes)} suffix={<InputClockIcon fill="#CCD5DD" />} disabled />
    )
  }

  return (
    <Popover
      className="kc-time-picker"
      placement="bottom"
      content={time}
      trigger="click"
      overlayStyle={{
        width: "7.5rem"
      }}
    >
      <Button
        className="kc-time-picker-btn"
        style={{width: width}}
      >
        {getTime(hours, minutes)}<InputClockIcon />
      </Button>
    </Popover>
  )
}

export default TimePicker;
