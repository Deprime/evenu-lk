import React, {useState} from "react";

const Loader = ({size = 'md'}) => {
  return (
    <div className="kc-loading kc-loader-logo" title="...Loading"></div>
  );
}

export default Loader;
