import React, {useState, useEffect} from "react";

const Input = ({
  size = 'md',
  nativeType = 'text',
  value = '',
  label = '',
  placeholder = '',
  disabled = false,
  onChange,
  ...props
}) => {

  const cssClassList = [
    'rounded',
    'border-2',
    'border-stone-200',
    'hover:border-stone-300',
    'focus:border-stone-200',
    'active:border-stone-200',
    'py-1',
    'px-2',
    'block',
    'w-full'
  ];
  const witLabel = (label.length > 0);

  // const [controlValue, setControlData] = useState(value);

  /**
   * Set conrtol data
   */
  const change = (e) => {

  }

  useEffect(() => {

  });

  return (
    <>
      <label htmlFor="" className="block w-full">
        {label}
      </label>
      <input
        type={nativeType}
        placeholder={placeholder}
        className={cssClassList.join(' ')}
        disabled={disabled}
        {...props}
        onChange={onChange}
      />
    </>
  );
}

export default Input;
