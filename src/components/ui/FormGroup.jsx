import React, {useState, useEffect} from "react";

const FormGroup = ({children}) => {
  return (
    <div className="mb-2">
      {children}
    </div>
  );
}

export default FormGroup;
