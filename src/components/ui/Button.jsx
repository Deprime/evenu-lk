import React, {useState, useEffect} from "react";

const Button = ({
  children,
  onClick,
  variant    = 'default',
  block      = false,
  disabled   = false,
  loading    = false,
  nativeType = 'button',
  ...props
}) => {

  // Base class list
  const baseClassList = [
    'btn',
    'rounded',
    'py-2',
    'px-4',
    'font-medium',
    block ? 'block w-full' : '',
    loading ? 'is-loading' : '',
  ];

  // Default variant
  const defaultClassList = [
    'bg-gray-200',
    'hover:bg-gray-300',
    'active:bg-gray-400',
    'text-gray-800',
  ];

  // Primary variant
  const primaryClassList = [
    'btn-primary',
    'bg-sky-500',
    'hover:bg-sky-600',
    'active:bg-sky-700',
    'text-white',
  ];

  const variantCollection = {
    default: defaultClassList,
    primary: primaryClassList
  }

  const cssClassList = (Object.keys(variantCollection).includes(variant))
                     ? [...baseClassList, ...variantCollection[variant]]
                     : [...baseClassList, ...defaultClassList];

  // Hover state
  const [hover, setHover ] = useState(false);
  const toggleHover = () => setHover(!hover);

  useEffect(() => {});

  return (
    <button
      type={nativeType}
      className={cssClassList.join(' ')}
      {...props}
      onMouseEnter={toggleHover}
      onMouseLeave={toggleHover}
      disabled={disabled}
      onClick={ !disabled && !loading ? onClick : () => {}}
    >
      {children}
    </button>
  );
}

export default Button;
