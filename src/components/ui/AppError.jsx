import React from "react";
import {useTranslation} from "react-i18next";

const AppError = () => {
  const { t } = useTranslation(["LMSErrors"]);

  return (
    <section className="kc-lms-error">
      <div className="kc-lms-error-inner">
        <div className="kc-logo"></div>
        <h2>{t("init-error", "LMS error")}</h2>
        <p>{t("init-error-description", "An error occurred during initialization of system components")}</p>
      </div>
    </section>
  );
}

export default AppError;
