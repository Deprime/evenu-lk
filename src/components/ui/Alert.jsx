import React, {useState, useEffect} from "react";

const Alert = ({
  children,
  variant = 'info',
  ...props
}) => {

  // Base class list
  const baseClassList = [
    'alert',
    'rounded',
    'py-3',
    'px-4',
    'border',
    'text-xs',
    'block w-full',
  ];

  // Info variant
  const infoClassList = [
    'alert-info',
    'bg-sky-50',
    'border-sky-200',
    'text-sky-900',
  ];

  // Danger variant
  const dangerClassList = [
    'alert-danger',
    'bg-red-50',
    'border-red-200',
    'text-red-900',
  ];

  const variantCollection = {
    info: infoClassList,
    danger: dangerClassList,
  }

  const cssClassList = (Object.keys(variantCollection).includes(variant))
                     ? [...baseClassList, ...variantCollection[variant]]
                     : [...baseClassList, ...infoClassList];

  // useEffect(() => {});

  return (
    <div
      className={cssClassList.join(' ')}
    >
      {children}
    </div>
  );
}

export default Alert;
