import React, {useState, useEffect} from "react";
import { Link as RouterLink } from "react-router-dom";

const Link = ({to, children, ...props}) => {

  // Base class list
  const baseClassList = [
    'link',
    'text-sky-700',
    'underline',
    'underline-offset-4',
    'decoration-1',
    'decoration-slate-300',
    'hover:decoration-slate-400'
  ];

  return (
    <RouterLink
      to={to}
      {...props}
      className={`${props.className} ${baseClassList.join(' ')}`}
    >
      {children}
    </RouterLink>
  );
}

export default Link;
