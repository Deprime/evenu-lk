import React from 'react';
import {BrowserRouter as Router, Switch, Route, Redirect} from "react-router-dom";

import '@src/config/i18n.js'
// import { useTranslation } from 'react-i18next'

// import Signin         from "@src/pages/Signin";
// import LogOut         from "@src/pages/LogOut.jsx";
import GuestLayout    from "@src/layouts/GuestLayout";
import PrivateLayout  from "@src/layouts/PrivateLayout";

const LmsApp = () => {
  // const {t, i18n} = useTranslation();
  return (
    <div className="app-wrapper text-sm">
      <Router>
        <Switch>
          {/* <Route path="/auth" exact component={Signin} key="r-auth" /> */}
          {/* <Route path="/logout" exacts component={LogOut} key="r-logout" /> */}

          <GuestLayout />
          <PrivateLayout />
        </Switch>
      </Router>
    </div>
  )
}

export default LmsApp
