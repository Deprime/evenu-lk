import React, {useEffect, useLayoutEffect, useState} from "react";
import store from "@src/store/Store.js";

const usePermissions = () => {
  const [permission, setPermission] = useState({})

  useEffect(() => {
    const obj = {
      isClasses: store.portal.get('isClasses'),
      isRequestAccess: store.portal.get('isRequestAccess'),
      isQuestionRepository: store.user.getAccountData('account_setting.quiz.isAddFromQuestionRepository'),
      isCoursesConfirmation: store.user.getAccountData('account_setting.isCoursesConfirmation'),
      superAdmin: store.user.isSuperAdmin(),
      portalManager: store.user.isSuperAdmin(),
      isTranslator: Boolean(store.user.getAccountData('options.is_translator')),
      isTrainingLimit: Boolean( store.user.getAccountData('account_setting.is_training_limit'))
    }
    setPermission(obj);
  }, [])

  return [permission, setPermission];
};

export {usePermissions};
