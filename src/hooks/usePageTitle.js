import React, { useEffect, useState } from "react";
const app_name = import.meta.env.VITE_APP_NAME

const usePageTitle = title => {
  const [doctitle, setPageTitle] = useState(title);

  useEffect(() => {
    document.title = `${doctitle} :: ${app_name}`;
  }, [doctitle]);

  return [doctitle, setPageTitle];
};

export {usePageTitle};
