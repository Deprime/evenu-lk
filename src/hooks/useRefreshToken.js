import React, { useEffect, useState } from "react";
import {useHistory, useLocation} from "react-router-dom";
import store from "@src/store/Store";
import Auth from "@src/services/api/Auth";

const useRefreshToken = () => {
  const history = useHistory();
  const location = useLocation();
  const token_refresh_rate = 20; // seconds

  useEffect(() => {
    refreshToken(null);
    const int_id = setInterval(() => refreshToken(int_id), token_refresh_rate * 1000);
  }, []);

  /**
   * Token refresh method
   * @returns {void}
   */
  async function refreshToken(int_id) {
    try {
      await Auth.refreshToken();
    } catch (e) {
      store.user.remove();

      if (int_id) {
        clearInterval(int_id);
      }

      // Redirect to auth page
      history.push({
        pathname: "./auth",
        state: {
          from: location.pathname
        }
      });
    }
  }
};

export {useRefreshToken};
