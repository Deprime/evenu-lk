# Notes

this file should be removed :)

## Auth\Admin does not have data hiding

Auth.js and AuthChecker.jsx related but not use one responsibility point.
AuthChecker is a component which means that it cannot directly affect the state of data warehouses lile:
```javascript
  async function refreshToken() {
    console.log('refreshToken');
    try {
      const result = await Auth.refreshToken();
    }
    catch (e) {
      store.user.setToken(null);
      useToggleModal(true);
    }
  }
```

To provide such operations, you should create a "User" entity, which will hide in itself both the work with the API, 
the storage of the token, and the authorization processes.

```puml
@startuml
[AuthChecker] ..> [User]
[User] ..> [Auth]
@enduml
```

## Filename convention

We use:
1. usePageTitle for hooks - camelCase
2. Sidebar.jsx for components - StudlyCase
3. routes.js - lowercase
4. icon-pack.scss - I don't know case :)

Filenames must be consistent!
1. basic CSS\SASS + Images, Fonts - lowercase (except Component.scss)
2. JS modules: StudlyCase?
3. folder names must be consistent! Or everything with a lower one or everything with a Studly one! 

## File tab convention

All js files should be 4 spaces.

## Coding standard and QA

1. Basic rules:
   1. camelCase for vars, StudlyCase for classes
   2. const vs var
   3. let vs var
   4. if(<>) { - curly brace for blocks on one line with a space always
   5. line break before blocks
2. Need to research: https://github.com/airbnb/javascript/tree/master/react#class-vs-reactcreateclass-vs-stateless
3. linter should be configured
4. unit test should be configured